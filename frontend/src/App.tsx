import { useEffect, useState } from 'react';
import './App.css';
import { Col, Container, Row, Table } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Base, PriceDetail } from './model/common';
import { getPriceSourcesByTicket, getAllPriceSources, getAllTickets } from './services/api-service';

function App() {
  const [priceSourceList, setPriceSourceList] = useState<Base[]>([]);
  const [ticketList, setTicketList] = useState<Base[]>([]);
  const [priceSelected, setPriceSelected] = useState('SRC1');
  const [ticketSelected, setTicketSelected] = useState('FPT SIN');
  const [priceSourcesByTicket, setPriceSourcesByTicket] = useState<PriceDetail[]>([]);

  function onPriceChange(event: any) {
    setPriceSelected(event.target.value);
    getPriceSourcesByTicket(event.target.value, ticketSelected).then(setPriceSourcesByTicket);
  };

  function onTicketChange(event: any) {
    setTicketSelected(event.target.value);
    getPriceSourcesByTicket(priceSelected, event.target.value).then(setPriceSourcesByTicket);
  };

  useEffect(() => {
    getAllPriceSources().then(setPriceSourceList);
    getAllTickets().then(setTicketList);
    getPriceSourcesByTicket(priceSelected, ticketSelected).then(setPriceSourcesByTicket);
  }, []);

  return (
    <div className='App'>

      <Container>
        <Row>
          <Col className='label'>Price source:</Col>
          <Col>
            <select className='source' value={priceSelected} onChange={onPriceChange}>
              {priceSourceList.map(option => (
                <option key={option.value} value={option.value}>
                  {option.label}
                </option>
              ))}
            </select>
          </Col>
        </Row>
        <br></br>
        <Row>
          <Col className='label'>Ticker:</Col>
          <Col>
            <select className='source' value={ticketSelected} onChange={onTicketChange}>
              {ticketList.map(option => (
                <option key={option.value} value={option.value}>
                  {option.label}
                </option>
              ))}
            </select></Col>
        </Row>
        <Table striped bordered hover size='sm'>
          <thead>
            <tr>
              <th>Time</th>
              <th>Price</th>
            </tr>
          </thead>
          <tbody>
            {priceSourcesByTicket.map((price, index) => (
              <tr key={index}>
                <td>
                  {price.createdDate ? price.createdDate : '2022-06-01 08:59:27'}
                </td>
                <td>
                  {price.price}
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </Container>
    </div>
  );
}

export default App;
