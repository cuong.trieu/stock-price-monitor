export type Base = {
  value: string;
  label: string;
};
export type PriceDetail = {
  createdDate: string;
  price: number;
};
