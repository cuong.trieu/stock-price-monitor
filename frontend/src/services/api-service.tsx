import { Base, PriceDetail } from "../model/common";

const API_BASE_URL = "https://localhost:5001";
export async function getAllPriceSources(): Promise<Base[]> {
  const result = await fetch(`${API_BASE_URL}/api/priceSource`);
  return result.json();
}
export async function getAllTickets(): Promise<Base[]> {
  const result = await fetch(`${API_BASE_URL}/api/ticket`);
  return result.json();
}
export async function getPriceSourcesByTicket(sourceValue: string, ticketValue: string): Promise<PriceDetail[]> {
  const result = await fetch(`${API_BASE_URL}/api/priceSource/ticket?sourceValue=${sourceValue}&ticketValue=${ticketValue}`);
  return result.json();
}
