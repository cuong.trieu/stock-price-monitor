using AutoMapper;
using ExampleApp.Controllers;
using ExampleApp.Data;
using ExampleApp.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ExampleApp.Tests
{
    public class PriceSourceControllerTests
    {
        private readonly MapperConfiguration _configuration;
        private readonly IMapper _mapper;
        private readonly ILogger<DataSeeder> _logger;

        public PriceSourceControllerTests()
        {
            _configuration = new MapperConfiguration(c => c.AddProfile<PriceProfile>());
            _mapper = _configuration.CreateMapper();
            _logger = Mock.Of<ILogger<DataSeeder>>();
        }

        [Fact]
        public void Configuration_IsValid()
        {
            _configuration.AssertConfigurationIsValid();
        }

        [Fact]
        public async Task Should_Get_All_Price_Sources()
        {
            //Arrange
            var dbContext = await GetAppDbContext();
            var userController = new PriceSourceController(dbContext, _mapper);
            //Act
            var response = await userController.PriceSources();
            //Assert
            Assert.Equal(2, response.ToList().Count);
        }
        private async Task<AppDbContext> GetAppDbContext()
        {
            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
            var AppDbContext = new AppDbContext(options);
            if (await AppDbContext.PriceSources.CountAsync() <= 0)
            {
                DataSeeder initalData = new DataSeeder(AppDbContext, _logger);
                initalData.Seed();
            }
            return AppDbContext;
        }
    }
}
