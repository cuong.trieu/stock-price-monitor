using ExampleApp.Entities;
using Microsoft.Extensions.Logging;
using System;

namespace ExampleApp.Data
{
    public class DataSeeder
    {
        private readonly AppDbContext _context;
        private readonly ILogger<DataSeeder> _logger;

        public DataSeeder(AppDbContext context, ILogger<DataSeeder> logger)
        {
            _context = context;
            _logger = logger;
        }

        public void Seed()
        {
            if (!_context.Database.EnsureCreated())
            {
                // Database already exists, don't try to seed it
                return;
            }

            _logger.LogInformation("Seeding database");

            var priceSource = new PriceSource[]
            {
                 new PriceSource { Value = "SRC1", Label = "Source 1"},
                 new PriceSource { Value = "SRC2", Label = "Source 2"}
            };

            var ticket = new Ticket[]
            {
                 new Ticket { Value = "IBM UN", Label = "IBM UN"},
                 new Ticket { Value = "APPLE VN", Label = "APPLE VN"},
                 new Ticket { Value = "FPT SIN", Label = "FPT SIN"}
            };

            var priceDetail = new PriceDetail[]
            {
                 new PriceDetail { CreatedDate = DateTime.Now.ToString(), Price = 139.87, SourceValue = "SRC1", TicketValue = "FPT SIN" },
                 new PriceDetail { CreatedDate = DateTime.Now.ToString(), Price = 139.81, SourceValue = "SRC1", TicketValue = "FPT SIN" },
                 new PriceDetail { CreatedDate = DateTime.Now.ToString(), Price = 138.95, SourceValue = "SRC1", TicketValue = "FPT SIN"  },
                 new PriceDetail { CreatedDate = DateTime.Now.ToString(), Price = 139.24, SourceValue = "SRC1", TicketValue = "FPT SIN"  },
                 new PriceDetail { CreatedDate = DateTime.Now.ToString(), Price = 139.33, SourceValue = "SRC1", TicketValue = "FPT SIN"  },
                 new PriceDetail { CreatedDate = DateTime.Now.ToString(), Price = 139.33, SourceValue = "SRC1", TicketValue = "APPLE VN" },
                 new PriceDetail { CreatedDate = DateTime.Now.ToString(), Price = 139.87, SourceValue = "SRC2", TicketValue = "APPLE VN" },
                 new PriceDetail { CreatedDate = DateTime.Now.ToString(), Price = 139.81, SourceValue = "SRC2", TicketValue = "APPLE VN" },
                 new PriceDetail { CreatedDate = DateTime.Now.ToString(), Price = 138.95, SourceValue = "SRC2", TicketValue = "APPLE VN" },
                 new PriceDetail { CreatedDate = DateTime.Now.ToString(), Price = 139.24, SourceValue = "SRC2", TicketValue = "APPLE VN" },
                 new PriceDetail { CreatedDate = DateTime.Now.ToString(), Price = 139.33, SourceValue = "SRC2", TicketValue = "APPLE VN" },
                 new PriceDetail { CreatedDate = DateTime.Now.ToString(), Price = 139.33, SourceValue = "SRC2", TicketValue = "APPLE VN" },
            };

            _context.AddRange(priceSource);
            _context.AddRange(ticket);
            _context.AddRange(priceDetail);
            _context.SaveChanges();

            _logger.LogInformation($"Added");
        }
    }
}
