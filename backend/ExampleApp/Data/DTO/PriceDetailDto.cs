using System;
using System.ComponentModel.DataAnnotations;

namespace ExampleApp.Data
{
    public class PriceDetailDto
    {
        string CreatedDate { get; set; }
        public double Price { get; set; }
    }
}
