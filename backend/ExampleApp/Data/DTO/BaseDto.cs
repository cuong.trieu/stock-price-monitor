using System.ComponentModel.DataAnnotations;

namespace ExampleApp.Data
{
    public class BaseDto
    {
        [Required]
        public string Value { get; set; }
        [Required]
        public string Label { get; set; }
    }
}
