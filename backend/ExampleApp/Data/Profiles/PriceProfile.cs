using AutoMapper;
using ExampleApp.Entities;

namespace ExampleApp.Data
{
    public class PriceProfile : Profile
    {
        public PriceProfile()
        {
            CreateMap<PriceSource, PriceSourceDto>(); 
            CreateMap<Ticket, TicketDto>();
            CreateMap<PriceDetail, PriceDetailDto>();
        }
    }
}
