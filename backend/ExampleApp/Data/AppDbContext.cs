using ExampleApp.Entities;
using Microsoft.EntityFrameworkCore;

namespace ExampleApp.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        { }

        public DbSet<PriceSource> PriceSources { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<PriceDetail> PriceDetails { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Set the entity relationship here.
        }
    }
}
