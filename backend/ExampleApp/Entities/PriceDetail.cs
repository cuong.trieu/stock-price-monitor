using System;
using System.ComponentModel.DataAnnotations;

namespace ExampleApp.Entities
{
    public class PriceDetail
    {
        [Key]
        public int Id { get; set; }
        public string SourceValue { get; set; }
        public string TicketValue { get; set; }
        public string CreatedDate { get; set; }
        public double Price { get; set; }
    }
}
