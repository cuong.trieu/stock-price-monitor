using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ExampleApp.Entities
{
    public class PriceSource
    {
        [Key]
        public int Id { get; set; }
        public string Value { get; set; }
        public string Label { get; set; }
    }
}
