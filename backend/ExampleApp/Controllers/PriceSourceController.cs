﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ExampleApp.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ExampleApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PriceSourceController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;

        public PriceSourceController(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Retrieves all the available PriceSources.
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<PriceSourceDto>> PriceSources()
        {
            var priceSources = _context.PriceSources;
            var priceSourceDtos = _mapper.ProjectTo<PriceSourceDto>(priceSources);
            return await priceSourceDtos.ToListAsync();
        }
        [HttpGet("ticket")]
        public async Task<IEnumerable<PriceDetailDto>> PriceSourcesByTicket(string sourceValue, string ticketValue)
        {
            var priceDetails = _context.PriceDetails.Where(x => x.TicketValue.ToLower().Equals(ticketValue.ToLower()) && x.SourceValue.ToLower().Equals(sourceValue.ToLower())).Take(5);
            var priceDetailsDtos = _mapper.ProjectTo<PriceDetailDto>(priceDetails);
            return await priceDetailsDtos.ToListAsync();        }
    }
}
