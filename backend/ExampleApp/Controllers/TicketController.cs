﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ExampleApp.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ExampleApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TicketController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;

        public TicketController(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Retrieves all the available Tickets.
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<TicketDto>> Tickets()
        {
            var tickets = _context.Tickets;
            var ticketDtos = _mapper.ProjectTo<TicketDto>(tickets);
            return await ticketDtos.ToListAsync();
        }
    }
}
